# The OpenMW Mod Repository
The OpenMW Mod Repository stores pybuilds, a variation on [Gentoo's Ebuilds](https://wiki.gentoo.org/wiki/Ebuild).
These build files can be used by Portmod to install OpenMW mods.

See [Portmod](https://gitlab.com/portmod/portmod/) for details on how to use this repository to install mods.

An overview of the format can be found on the [Portmod Wiki](https://gitlab.com/portmod/portmod/wikis/).
