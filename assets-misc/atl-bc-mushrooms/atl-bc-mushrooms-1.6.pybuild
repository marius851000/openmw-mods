# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir
from pyclass import NexusMod


class Mod(NexusMod, Pybuild1):
    NAME = "Project Atlas BC Mushrooms"
    DESC = 'Replaces the meshes in the "BC Mushrooms" set with new, atlased versions'
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45399"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "openmw"
    SRC_URI = """
        ATL_BC_Mushrooms-45399-1-6-1555717707.7z
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45399"
    TEXTURE_SIZES = """
        smooth? ( 256 )
        !smooth? ( 362 )
    """
    RDEPEND = """
        glowing-bitter-coast? ( assets-misc/glowing-bitter-cast )
        !glowing-bitter-coast? ( !!assets-misc/glowing-bitter-cast )
    """
    DATA_OVERRIDES = """
        assets-misc/morrowind-optimization-patch
        assets-meshes/properly-smoothed-meshes
    """
    IUSE = "smooth glowing-bitter-coast"
    TIER = 1
    INSTALL_DIRS = [
        InstallDir(
            "00 Core - Smoothed Meshes",
            S="ATL_BC_Mushrooms-45399-1-6-1555717707",
            REQUIRED_USE="smooth",
        ),
        InstallDir(
            "01 Glowing Bitter Coast Patch for Smoothed Meshes",
            S="ATL_BC_Mushrooms-45399-1-6-1555717707",
            REQUIRED_USE="smooth glowing-bitter-coast",
        ),
        InstallDir(
            "00 Core - Normal Meshes",
            S="ATL_BC_Mushrooms-45399-1-6-1555717707",
            REQUIRED_USE="!smooth",
        ),
        InstallDir(
            "01 Glowing Bitter Coast Patch for Normal Meshes",
            S="ATL_BC_Mushrooms-45399-1-6-1555717707",
            REQUIRED_USE="!smooth glowing-bitter-coast",
        ),
    ]
