# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir
from pyclass import NexusMod

MAINFILE = "Morrowind_VanillaPlus_textures-45813-1-2"
PURISTFILE = "Morrowind_VanillaPlus_textures_-_Purist_Edition-45813-1-2"
ARMORWEAPONS = "Armor_Weapons_and_Clothes-45813-1-2"


class Mod(Pybuild1, NexusMod):
    NAME = "Morrowind VanillaPlus Textures"
    DESC = "Gives most vanilla terrain textures 4x resolution and texture maps"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45813"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45813"
    TIER = "1"
    SRC_URI = f"""
        !purist? ( {MAINFILE}.zip )
        purist? ( {PURISTFILE}.zip )
        armor-weapons-clothes? ( {ARMORWEAPONS}.zip )
    """
    IUSE = "purist +armor-weapons-clothes"
    RESTRICT = "fetch"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "openmw"
    INSTALL_DIRS = [
        InstallDir(".", S=MAINFILE, BLACKLIST=["__MACOSX"], REQUIRED_USE="!purist"),
        InstallDir(".", S=PURISTFILE, BLACKLIST=["__MACOSX"], REQUIRED_USE="purist"),
        InstallDir(
            ".",
            S=ARMORWEAPONS,
            BLACKLIST=["__MACOSX", "normalmaps-add content to your textures folder"],
            REQUIRED_USE="armor-weapons-clothes",
        ),
        InstallDir(
            "normalmaps-add content to your textures folder",
            RENAME="Textures",
            S=ARMORWEAPONS,
            REQUIRED_USE="armor-weapons-clothes !purist",
        ),
    ]
