# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from portmod.pybuild import Pybuild1, InstallDir, File
from pyclass import CleanPlugin, NexusMod, CLEAN_DEPEND


class Mod(CleanPlugin, NexusMod, Pybuild1):
    NAME = "Taddeus On the Rocks with normal maps for OpenMW"
    DESC = "A pluginless rock replacer"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45140"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    DEPEND = f"""
        base/morrowind
        coast-variety? (
            {CLEAN_DEPEND}
        )
    """
    KEYWORDS = "openmw"
    SRC_URI = """
        !all? (
            bitter-coast? ( 01_Bitter_Coast-45140-1-0.7z )
            azuras-coast? ( 02_Azuras_Coast-45140-1-0.7z )
            grazelands? ( 03_Grazelands-45140-1-0.7z )
            west-gash? ( 04_West_Gash-45140-1-0.7z )
            ascadian-islands? ( 06_Ascadian_Islands-45140-1-0.7z )
            red-mountain? ( 07_Red_Mountain-45140-1-0.7z )
            ashlands? ( 08_Ashlands-45140-1-0.7z )
            molag-amur? ( 09_Molag_Amur-45140-1-0.7z )
            solstheim? ( 10_Solstheim-45140-1-0.7z )
            coast-variety? ( 11_Coast_Variety-45140-1-0.7z )
        )
        all? ( Complete_pack-45140-1-0.7z )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45140"
    TEXTURE_SIZES = "1024"
    IUSE = """
        +all
        +bitter-coast
        +azuras-coast
        +grazelands
        +west-gash
        +ascadian-islands
        +red-mountain
        +ashlands
        +molag-amur
        +solstheim
        +coast-variety
    """
    REQUIRED_USE = """
        all? (
            bitter-coast
            azuras-coast
            grazelands
            west-gash
            ascadian-islands
            red-mountain
            ashlands
            molag-amur
            solstheim
            coast-variety
        )
    """
    INSTALL_DIRS = [
        InstallDir(
            ".", S="01_Bitter_Coast-45140-1-0", REQUIRED_USE="!all bitter-coast"
        ),
        InstallDir(
            ".", S="02_Azuras_Coast-45140-1-0", REQUIRED_USE="!all azuras-coast"
        ),
        InstallDir(".", S="03_Grazelands-45140-1-0", REQUIRED_USE="!all grazelands"),
        InstallDir(".", S="04_West_Gash-45140-1-0", REQUIRED_USE="!all west-gash"),
        InstallDir(
            ".", S="06_Ascadian_Islands-45140-1-0", REQUIRED_USE="!all ascadian-islands"
        ),
        InstallDir(
            ".", S="07_Red_Mountain-45140-1-0", REQUIRED_USE="!all red-mountain"
        ),
        InstallDir(".", S="08_Ashlands-45140-1-0", REQUIRED_USE="!all ashlands"),
        InstallDir(".", S="09_Molag_Amur-45140-1-0", REQUIRED_USE="!all molag-amur"),
        InstallDir(
            ".",
            PLUGINS=[File("OTR_Coast_Variety.esp")],
            S="11_Coast_Variety-45140-1-0",
            REQUIRED_USE="!all coast-variety",
        ),
        InstallDir(".", S="10_Solstheim-45140-1-0", REQUIRED_USE="!all solstheim"),
        InstallDir(
            ".",
            PLUGINS=[File("OTR_Coast_Variety.esp")],
            S="Complete_pack-45140-1-0",
            REQUIRED_USE="all",
        ),
    ]

    def src_prepare(self):
        if "all" in self.USE:
            self.clean_plugin("OTR_Coast_Variety.esp")
        elif "coast-variety" in self.USE:
            self.clean_plugin(
                os.path.join(
                    self.WORKDIR, "11_Coast_Variety-45140-1-0", "OTR_Coast_Variety.esp"
                )
            )
