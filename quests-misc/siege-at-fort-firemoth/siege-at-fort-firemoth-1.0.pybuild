# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from portmod.pybuild import Pybuild1, InstallDir, File, apply_patch


class Mod(Pybuild1):
    NAME = "Siege at Firemoth"
    DESC = "Adds a small island chain and a quest involving a large army of skeletons"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    # Not necessarily a hard blocker for morrowind-rebirth, but the minimal version
    # increases compatability and should be used with it
    RDEPEND = """
        base/morrowind
        !minimal? ( !!base/morrowind-rebirth )
    """
    KEYWORDS = "openmw"
    IUSE = "minimal"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/firemoth1.1.zip
        https://gitlab.com/bmwinger/umopp/uploads/1098845ebed5bbe8e718371c04fde757/firemoth-umopp-3.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data Files", PLUGINS=[File("Siege at Firemoth.esp")], S="firemoth1.1"
        )
    ]

    def src_prepare(self):
        # From instructions in README.md
        os.chdir("Data Files")
        path = os.path.join(self.WORKDIR, "firemoth-umopp-3.0.2")
        if "minimal" in self.USE:
            apply_patch(os.path.join(path, "Siege_at_Firemoth_compat_plugin.patch"))
        else:
            apply_patch(os.path.join(path, "Siege_at_Firemoth_plugin.patch"))
